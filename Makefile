SHELL:=/bin/bash
.DEFAULT_GOAL := help

#
# main variables
#

PROJECT_NAME=blackbox-exporter
VERSION ?= $(shell git describe --always)
BLACKBOX_VERSION ?= 0.24.0
ARCH=amd64
RPM_NAME = cactus-blackbox-exporter-${VERSION}-${RELEASE}.${ARCH}.rpm

#
# main targets
#

.PHONY: rpm
rpm: ${RPM_NAME} ## Creates RPM packages for this project

.PHONY: clean
clean: ## Cleans up project
	rm -rf ${RPM_NAME}

#
# dependencies
#

BLACKBOX_STR = blackbox_exporter-${BLACKBOX_VERSION}.linux-${ARCH}
${BLACKBOX_STR}:
	curl -LO https://github.com/prometheus/blackbox_exporter/releases/download/v${BLACKBOX_VERSION}/${BLACKBOX_STR}.tar.gz
	tar xf ${BLACKBOX_STR}.tar.gz
	rm -rf ${BLACKBOX_STR}.tar.gz
	touch  ${BLACKBOX_STR}
	

${RPM_NAME}: ${BLACKBOX_STR} *
	rm -rf rpmroot
	mkdir -p rpmroot/opt/cactus/bin/blackbox-exporter rpmroot/usr/lib/systemd/system rpmroot/opt/cactus/etc/blackbox-exporter
	cp systemd/* rpmroot/usr/lib/systemd/system
	cp conf.* rpmroot/opt/cactus/etc/blackbox-exporter
	cp ${BLACKBOX_STR}/blackbox_exporter rpmroot/opt/cactus/bin/blackbox-exporter/blackbox-exporter

	mkdir -p rpms
	cd rpmroot && fpm \
	-s dir \
	-t rpm \
	-n cactus-${PROJECT_NAME} \
	-v ${VERSION} \
	-a ${ARCH} \
	-m "<cactus@cern.ch>" \
	--vendor CERN \
	--description "Blackbox exporter for the L1 Online Software at P5" \
	--url "https://gitlab.cern.ch/cms-cactus/ops/monitoring/blackbox-exporter" \
	--provides cactus-${PROJECT_NAME} \
	.=/ && mv *.rpm ../rpms/

.PHONY: help
help: ## Displays this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
