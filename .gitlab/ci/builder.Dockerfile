FROM prom/blackbox-exporter:v0.18.0
COPY conf.P5.yaml /etc/blackbox_exporter/config.yml
EXPOSE 9115